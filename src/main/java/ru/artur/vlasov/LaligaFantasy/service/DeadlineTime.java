package ru.artur.vlasov.LaligaFantasy.service;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.artur.vlasov.LaligaFantasy.model.DeadlineDto;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class DeadlineTime {

    public String getDeadlineTime(DeadlineDto deadlineDto, String urlTime, String urlTeams) throws IOException {
        URL DeadLine = new URL(urlTime);
        Scanner in = new Scanner((InputStream) DeadLine.getContent());

        URL urlTeamsWithoutSquad = new URL(urlTeams);
        Scanner inTeams = new Scanner((InputStream) urlTeamsWithoutSquad.getContent());

        StringBuilder resultDeadline = new StringBuilder();
        while (in.hasNext()) {
            resultDeadline.append(in.nextLine());
        }

        StringBuilder resultTeamsWithoutSquad = new StringBuilder();
        while (inTeams.hasNext()) {
            resultTeamsWithoutSquad.append(inTeams.nextLine());
        }

        JSONObject object = new JSONObject(resultDeadline.toString());
        JSONArray jsonArray = new JSONArray(resultTeamsWithoutSquad.toString());

        StringBuilder teamsInfo = new StringBuilder("Команды без составов:\n");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject teamObject = jsonArray.getJSONObject(i);
            String teamName = teamObject.getString("teamName");
            String telegramId = teamObject.getString("telegramId");

            teamsInfo.append(teamName).append(" (").append(telegramId).append(")").append("\n");
        }

        deadlineDto.setTour(object.getInt("tour"));
        deadlineDto.setDeadline(object.getString("deadlineDto"));
        deadlineDto.setSecondsBeforeTour(object.getLong("secondsBeforeTour"));
        return "Номер тура : " + deadlineDto.getTour() + "\n" +
                "Время deadlineDto : " + deadlineDto.getDeadline() + "\n" +
                "До deadlineDto : " + (deadlineDto.getSecondsBeforeTour() / 3600) + " часов"+ "\n" +
                "\n" +
                teamsInfo + "\n";
    }
}