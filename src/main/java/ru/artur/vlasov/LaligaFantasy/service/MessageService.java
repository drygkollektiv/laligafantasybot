package ru.artur.vlasov.LaligaFantasy.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class MessageService {
    private static final String EPL_URL = "http://epl.soccerfantasy.ru/api/player?link=";
    private static final String LALIGA_URL = "http://laliga.soccerfantasy.ru/api/player?link=";

    protected void handleUserMessage(Message message, TelegramBot bot) throws IOException {
        String messageText = message.getText();
        long chatId = message.getChatId();

        if (messageText.contains("htt") && messageText.contains("EPL")) {
            bot.startCommandReceived(chatId, extractLinkFromMessage(message.getText()), EPL_URL);
        } else if (messageText.contains("htt") && messageText.contains("LALIGA")) {
            bot.startCommandReceived(chatId, extractLinkFromMessage(message.getText()), LALIGA_URL);
        } else {
            bot.sendMessage(chatId, "Отправьте ссылку на sports.ru");
        }
    }

    protected void handleGroupMessage(Message message, TelegramBot bot) throws IOException {
        String messageText = message.getText();
        long chatId = message.getChatId();

        if (messageText.contains("htt") && chatId == -1001057666677L) {
            bot.startCommandReceived(chatId, extractLinkFromMessage(message.getText()), EPL_URL);
        } else if (messageText.contains("htt") && chatId == -1001148108113L) {
            bot.startCommandReceived(chatId, extractLinkFromMessage(message.getText()), LALIGA_URL);
        } else if (messageText.contains("result")) {
            sendToursResult(message, bot);
        } else if (messageText.contains("table")) {
            sendActualTournamentTable(message, bot);
        }
    }

    protected String extractLinkFromMessage(String message) {

        Pattern pattern = Pattern.compile("htt\\S+");
        Matcher matcher = pattern.matcher(message);

        if (matcher.find()) {
            return matcher.group();
        }
        return "ссылка не верная";
    }

    protected void sendToursResult(Message message, TelegramBot bot) {
        long chatId = message.getChatId();
        String text = message.getText();
        int tourNumberFromMessage = Integer.parseInt(text.replaceAll("\\D", ""));

        if (chatId == -1001148108113L) {
            bot.sendMessage(chatId, TourResult.getTourResult(tourNumberFromMessage, "http://laliga.soccerfantasy.ru"));
        } else if (chatId == -1001057666677L) {
            bot.sendMessage(chatId, TourResult.getTourResult(tourNumberFromMessage, "http://epl.soccerfantasy.ru"));
        }
    }

    protected void sendActualTournamentTable(Message message, TelegramBot bot) {
        long chatId = message.getChatId();

        if (chatId == -1001148108113L) {
            bot.sendMessage(chatId, TournamentTable.getTournamentTable("http://laliga.soccerfantasy.ru"));
        } else if (chatId == -1001057666677L) {
            bot.sendMessage(chatId, TournamentTable.getTournamentTable("http://epl.soccerfantasy.ru"));
        }
    }
}
