package ru.artur.vlasov.LaligaFantasy.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TournamentTable {
    private static final Logger log = LoggerFactory.getLogger(TournamentTable.class);

    public static String getTournamentTable(String leagueUrl) {
        StringBuilder message = new StringBuilder();

        try {
            Document document = Jsoup.connect(leagueUrl + "/table-fantasy/index?seasonId=11").get();
            Element tbody = document.select("tbody").first();
            assert tbody != null;
            Elements rows = tbody.select("tr");
            for (Element row : rows) {
                Elements cells = row.select("td:lt(6)");
                if (cells.size() >= 6) {
                    String column1 = cells.get(0).text();
                    String column2 = cells.get(1).text();
                    String column3 = cells.get(2).text();

                    message.append(column1).append(" ");
                    message.append(column2).append(" ");
                    message.append(column3).append("\n");

                } else {
                    message.append("Ошибка");
                }
            }
        } catch (Exception e) {
            log.error("An error occurred: ", e);
        }
        return message.toString();
    }
}

