package ru.artur.vlasov.LaligaFantasy.service;

import org.json.JSONObject;
import ru.artur.vlasov.LaligaFantasy.model.PlayerDto;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class GetFootballersInfoFromFantasy {
    public static String getFootballersInfoFromFantasy(String message, PlayerDto playerDto, String leagueUrl) throws IOException {

        URL url = new URL(leagueUrl + message);
        Scanner in = new Scanner((InputStream) url.getContent());
        StringBuilder result = new StringBuilder();

        while (in.hasNext()) {
            result.append(in.nextLine());
        }
        if (!result.toString().startsWith("{")) {
            return "По этой ссылке игрока в базе нет. Либо ссылка не корректная.";
        }

        JSONObject object = new JSONObject(result.toString());

        playerDto.setName(object.getString("name"));
        playerDto.setPosition(object.getString("position"));
        playerDto.setTeam(object.getString("team"));
        playerDto.setTeamFantasy(object.getString("teamFantasy"));
        return "Имя : " + playerDto.getName() + "\n" +
                "Позиция : " + playerDto.getPosition() + "\n" +
                "Команда в реальности : " + playerDto.getTeam() + "\n" +
                "Команда Fantasy : " + playerDto.getTeamFantasy() + "\n";
    }
}
