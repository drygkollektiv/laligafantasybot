package ru.artur.vlasov.LaligaFantasy.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.artur.vlasov.LaligaFantasy.config.BotConfig;
import ru.artur.vlasov.LaligaFantasy.model.DeadlineDto;
import ru.artur.vlasov.LaligaFantasy.model.PlayerDto;

import java.io.IOException;

@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {
    final BotConfig config;
    private final MessageService messageService;

    @Autowired
    public TelegramBot(BotConfig config, MessageService messageService) {
        this.config = config;
        this.messageService = messageService;
    }

    @Override
    public String getBotUsername() {
        return config.getBotName();
    }

    @Override
    public String getBotToken() {
        return config.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();

        if (update.hasMessage() && update.getMessage().hasText()) {
            try {
                if (message.isUserMessage()) {
                    messageService.handleUserMessage(message, this);
                } else if (message.getText().startsWith("@" + getBotUsername())) {
                    messageService.handleGroupMessage(message, this);
                }
            } catch (IOException e) {
                log.error("An error occurred: ", e);
            }
        }
    }


    void startCommandReceived(long chatId, String message, String leagueUrl) throws IOException {
        PlayerDto playerDto = new PlayerDto();
        String answer = GetFootballersInfoFromFantasy.getFootballersInfoFromFantasy(message, playerDto, leagueUrl);

        sendMessage(chatId, answer);
    }

    void sendMessage(long chatId, String textToSend) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(textToSend);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error("An error occurred: ", e);
        }

    }

    // обработка исключения (тут будут писаться логи)
    private void executeMessage(SendMessage message) {
        try {
            execute(message);
        } catch (TelegramApiException ignored) {

        }
    }

    // метод отправки сообщения для дедлайна
    private void prepareAndSendMessage(long chatId, String textToSend) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(textToSend);
        executeMessage(message);
    }


    @Scheduled(cron = "0 0 * * * *")
    private void eplDeadlineScheduler() throws IOException {
        long eplGroupChatId = -1001057666677L;
        String eplDeadlineURL = "https://epl.soccerfantasy.ru/api/deadline";
        String eplTeamsURL = "https://epl.soccerfantasy.ru/api/teams-without-squad";
        deadLineInfoSender(eplGroupChatId, eplDeadlineURL, eplTeamsURL);
    }

    @Scheduled(cron = "0 0 * * * *")
    private void laLigaDeadlineScheduler() throws IOException {
        long laligaGroupChatId = -1001148108113L;
        String laLigaDeadlineURL = "https://laliga.soccerfantasy.ru/api/deadline";
        String laLigaTeamsURL = "https://laliga.soccerfantasy.ru/api/teams-without-squad";
        deadLineInfoSender(laligaGroupChatId, laLigaDeadlineURL, laLigaTeamsURL);

    }

    public void deadLineInfoSender(long chatId, String urlTime, String urlTeams) throws IOException {
        DeadlineDto deadlineDto = new DeadlineDto();
        DeadlineTime deadlineTime = new DeadlineTime();
        String answer = deadlineTime.getDeadlineTime(deadlineDto, urlTime, urlTeams);
        if ((deadlineDto.getSecondsBeforeTour() / 3600) == 24) {
            prepareAndSendMessage(chatId, answer);
        } else if ((deadlineDto.getSecondsBeforeTour() / 3600) == 12) {
            prepareAndSendMessage(chatId, answer);
        } else if ((deadlineDto.getSecondsBeforeTour() / 3600) == 6) {
            prepareAndSendMessage(chatId, answer);
        }
    }
}
