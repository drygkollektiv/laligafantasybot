package ru.artur.vlasov.LaligaFantasy.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PlayerDto {
    String name;
    String position;
    String team;
    String teamFantasy;

}
